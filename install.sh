#!/bin/sh

source init/makeIn.sh

# .emacs.d
if [ ! -e ~/.emacs.d ]; then
    ln -fis ~/dotfiles/.emacs.d ~/.emacs.d
fi


#
# zshrc
#
if [ -e ~/.zshrc ]; then
    echo "source ~/dotfiles/zshrc" >> ~/.zshrc
    chmod 744 zshrc
    source ~/.zshrc
fi

if [ ! -e ~/.zprezto ]; then
    # zprezto インストール
    # TODO
    echo "you need install zprezto : https://github.com/sorin-ionescu/prezto"
    echo "README の手順でインストールすべし."
    exit
fi
