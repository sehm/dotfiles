#!/bin/sh

# ドットファイルのリンクをホームディレクトリに作る
cd $(dirname $0)
for dotfile in .?*
do
    if [ $dotfile != '..' ] && [ $dotfile != '.git' ] && [ -f $dotfile ]; then
        ln -Fis "$PWD/$dotfile" $HOME
    fi
done
