# ------------------------------------------------------
# オレオレ .zshrc
# ------------------------------------------------------

# root のときは英語になるようにする.
export LANG=ja_JP.UTF-8
case ${UID} in
0)
    LANG=C
    ;;
esac

# 日本語ファイル名を表示可能にする
setopt print_eight_bit

# '#' 以降をコメントとして扱う
setopt interactive_comments


# ------------------------------
#     alias
# ------------------------------
# グローバルエイリアス
# alias -g L='| less'
# alias -g G='| grep'

if [ "$(uname)" = 'Darwin' ]; then
    export LSCOLORS=gxfxcxdxbxgxdxabagacad
    alias ls='ls -Gl'

    # zsh の補完でも LSCOLORS の色を使用する.
    # 最終的な色はターミナルの設定で変わる　ターミナル -> 設定 -> テキスト -> ANSI カラー　の部分の色を変えるべし.
    zstyle ':completion:*' list-colors $LSCOLORS
else    
    eval `dircolors ~/.colorrc`    
    alias ls='ls --color=auto'
fi


# ------------------------------
#     プラグインごとの設定
# ------------------------------

# rbrew の設定
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi


# ------------------------------
#     キーバインド
# ------------------------------


# ------------------------------
#     環境ごとの設定
# ------------------------------
# mac 用の設定は zshrc.Darwin,  linux 用の設定は zshrc.Linux に書く
[ -f ~/dotfiles/zshrc.`uname` ] && source ~/dotfiles/zshrc.`uname`

# マシンごとの設定は以下のファイルに書く
[ -f ~/.zshrc.local ] && source ~/.zshrc.local
