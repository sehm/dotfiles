;;
;; Carbon Emacs 設定ファイル
;;

;; 読み込みパスを追加
(let ((default-directory (expand-file-name "~/.emacs.d/site-lisp")))
  (add-to-list 'load-path default-directory)
  (if (fboundp 'normal-top-level-add-subdirs-to-load-path)
      (normal-top-level-add-subdirs-to-load-path)))

;; 実行環境判別用の定数定義.  (when ns-p)  というふうに使うべし.
(setq darwin-p (eq system-type 'darwin)
      ns-p (eq window-system 'ns)
      carbon-p (eq window-system 'mac)
      linux-p (eq system-type 'gnu/linux)
      ;;colinux-p (when linux-p (let ((file "/proc/modules")) (and (file-readable-p file) (x->bool (with-temp-buffer (insert-file-contents file) (goto-char (point-min)) (re-search-forward "^cofuse\.+" nil t))))))
      cygwin-p (eq system-type 'cygwin) nt-p (eq system-type 'windows-nt)
      meadow-p (featurep 'meadow)
      windows-p (or cygwin-p nt-p meadow-p))

;;
;; 基本
;;
;; デフォルト文字コード
(set-default-coding-systems 'utf-8)

;; 初期フレームの設定
(setq default-frame-alist
      (append (list '(foreground-color . "White");;"black")
                    '(background-color . "Black");;"gray95")
		    '(border-color . "black")
		    '(mouse-color . "white")
		    '(cursor-color . "Gray");;"MediumPurple2")
;;		    '(font . "bdf-fontset")    ; BDF
;;		    '(font . "private-fontset"); TrueType
		    '(width . 230)
		    '(height . 88)
;;		    '(width . 260)
;;		    '(height . 70)
		    '(top . 10)
		    '(left . 10))
	      default-frame-alist))

;; デフォルトの透明度を設定する
(add-to-list 'default-frame-alist '(alpha . 85))

;; カレントウィンドウの透明度を変更する (85%)
;; (set-frame-parameter nil 'alpha 0.85)
(set-frame-parameter nil 'alpha 85)


;; M をとる
(add-hook 'comint-output-filter-functions 'shell-strip-ctrl-m nil t)

;;; メニュー, ツールバー非表示
(menu-bar-mode -1)
(when ns-p
  (tool-bar-mode -1))

;;; 括弧をハイライトする
(show-paren-mode t)

;;; window 分割時、画面外に出る文章を折り返す
(setq truncate-partial-width-windows nil)

;;; カーソルのある行をハイライト
;; (defface hlline-face
;;   '((((class color)
;;       (background dark))
;;      ;;(:background "dark state gray"))
;;      (:background "gray10"
;;                   :underline "gray24"))
;;     (((class color)
;;       (background light))
;;      (:background "ForestGreen"
;;                   :underline nil))
;;     (t ()))
;;   "*Face used by hl-line.")
;; (setq hl-line-face 'hlline-face)
;; ;;(setq hl-line-face 'underline)
;; (global-hl-line-mode)

;;; yank した文字列をハイライト表示
(when (or window-system (eq emacs-major-version '21))
  (defadvice yank (after ys:highlight-string activate)
    (let ((ol (make-overlay (mark t) (point))))
      (overlay-put ol 'face 'highlight)
      (sit-for 0.5)
      (delete-overlay ol)))
  (defadvice yank-pop (after ys:highlight-string activate)
    (when (eq last-command 'yank)
      (let ((ol (make-overlay (mark t) (point))))
        (overlay-put ol 'face 'highlight)
        (sit-for 0.5)
        (delete-overlay ol)))))

;;; Mac とクリップボード共有
(set-clipboard-coding-system 'sjis-mac)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; fixed-width-fontset でフォント設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;(if (eq window-system 'mac) (require 'carbon-font))
;;(fixed-width-set-fontset "hirakaku_w3" 10)

(set-face-attribute 'default nil :family "Ricty" :height 120)
(when ns-p
  (set-fontset-font "fontset-default" 'japanese-jisx0208 '("Ricty" . "iso10646-*")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; キー設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 全自動インデントを無効
(setq c-auto-newline nil)

;; MAC 専用 : Command-Key and Option-Key
(setq ns-command-modifier (quote meta))
(setq ns-alternate-modifier (quote meta))

;; 行頭以外でTABを入力できる（行頭ではインデント)
(setq c-tab-always-indent nil)

;;; TABをスペースにする
(setq-default indent-tabs-mode nil)

;;; C-k で改行も含めてカット
(setq kill-whole-line t)

;;; C-h でバックスペース
(global-set-key "\C-h" 'delete-backward-char)

;;; C-x p で C-x o の逆の動作
(define-key ctl-x-map "p"
  #'(lambda (arg) (interactive "p") (other-window (- arg))))

;;; ファイル名等の補完で大文字と小文字を区別させないようにする
(setq completion-ignore-case t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; shell-mode 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; コマンド検索パスに追加
(setq exec-path (append (list "/opt/local/bin")
                        exec-path))

;; パスワードの入力を隠す
(add-hook 'comint-output-filter-functions
          'comint-watch-for-password-prompt)

;;
(autoload 'ansi-color-for-comint-mode-on "ansi-color"
          "Set `ansi-color-for-comint-mode' to t." t)
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; whitespace.el
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ここの設定 http://syohex.hatenablog.com/entry/20110119/1295450495 
;; for whitespace-mode
(require 'whitespace)
;; see whitespace.el for more details
(setq whitespace-style '(face tabs tab-mark spaces space-mark))
(setq whitespace-display-mappings
      '((space-mark ?\u3000 [?\u25a1])
        ;; WARNING: the mapping below has a problem.
        ;; When a TAB occupies exactly one column, it will display the
        ;; character ?\xBB at that column followed by a TAB which goes to
        ;; the next TAB column.
        ;; If this is a problem for you, please, comment the line below.
        (tab-mark ?\t [?\xBB ?\t] [?\\ ?\t])))
(setq whitespace-space-regexp "\\(\u3000+\\)")
(set-face-foreground 'whitespace-tab "#adff2f")
(set-face-background 'whitespace-tab 'nil)
(set-face-underline  'whitespace-tab t)
(set-face-foreground 'whitespace-space "#7cfc00")
(set-face-background 'whitespace-space 'nil)
(set-face-bold-p 'whitespace-space t)
(global-whitespace-mode 1)
(global-set-key (kbd "C-x w") 'global-whitespace-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; shell-command.el 設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (require 'shell-command)
;; (shell-command-completion-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; browse-kill-ring 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'browse-kill-ring)
(global-set-key "\M-y" 'browse-kill-ring)

;; browse-kill-ring 終了時にバッファを kill する
(setq browse-kill-ring-quit-action 'kill-and-delete-window)

;; 現在選択中の kill-ring のハイライトする
(setq browse-kill-ring-highlight-current-entry t)

;; C-g で終了
(add-hook 'browse-kill-ring-hook
	  (lambda ()
	    (define-key browse-kill-ring-mode-map (kbd "\C-g") 'browse-kill-ring-quit)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; auto-complete
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-to-list 'load-path "~/dotfiles/site-lisp/")
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/dotfiles/site-lisp/ac-dict")
(ac-config-default)

;; 自動的に補完しない
(setq ac-auto-start nil)
(global-set-key "\M-/" 'auto-complete)

;; 補完を中止する
(define-key ac-completing-map "\M-/" 'ac-stop)

;; 大文字・小文字を区別しない
(setq ac-ignore-case t)

;; 
(ac-flyspell-workaround)

;; 自動でモード有効 追加
(add-to-list 'ac-modes 'csharp-mode)
(add-to-list 'ac-modes 'objc-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; linum : 行番号を表示するスクリプト
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'linum)
(global-linum-mode t)
(setq linum-format "%5d ")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; iswitchb.el 設定 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(iswitchb-mode 1)
;;; C-f, C-b, C-n, C-p で候補を切り替えることができるように。
(add-hook 'iswitchb-define-mode-map-hook
      (lambda ()
        (define-key iswitchb-mode-map "\C-n" 'iswitchb-next-match)
        (define-key iswitchb-mode-map "\C-p" 'iswitchb-prev-match)
        (define-key iswitchb-mode-map "\C-f" 'iswitchb-next-match)
        (define-key iswitchb-mode-map "\C-b" 'iswitchb-prev-match)))

;;; iswitchbで補完対象に含めないバッファ
;; (setq iswitchb-buffer-ignore
;;       '(
;;         "*twittering-wget-buffer*"
;;         "*twittering-http-buffer*"
;;         "*WoMan-Log*"
;;        "*SKK annotation*"
;;        "*Completions*"
;;         ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; recentf-ext.el
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'recentf-ext)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; http://www.bookshelf.jp/elc/summarye.el
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(autoload 'se/make-summary-buffer "summarye" nil t)

;; ;; ショートカットキー : C-c l で se/make-summary-buffer 
;; (define-key mode-specific-map "l" 'se/make-summary-buffer)
(global-set-key "\C-xl" 'se/make-summary-buffer)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; bm.el
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq-default bm-buffer-persistence nil)
(setq bm-restore-repository-on-load t)
(require 'bm)
(add-hook 'find-file-hooks 'bm-buffer-restore)
(add-hook 'kill-buffer-hook 'bm-buffer-save)
(add-hook 'after-save-hook 'bm-buffer-save)
(add-hook 'after-revert-hook 'bm-buffer-restore)
(add-hook 'vc-before-checkin-hook 'bm-buffer-save)
(global-set-key (kbd "M-SPC") 'bm-toggle)
(global-set-key (kbd "M-;") 'bm-previous)
(global-set-key (kbd "M-:") 'bm-next)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; navi.el : http://www.ne.jp/asahi/love/suna/pub/soft/navi.el/index.html
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (setq load-path (append load-path "/home/suna/lisp"))

;; (load-library "navi")
;; (global-set-key [f11] 'call-navi)
;; (global-set-key "\C-x\C-l" 'call-navi)
;; (defun call-navi ()
;;   (interactive)
;;   (navi (buffer-name)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; grep-edit.el
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (require 'grep)
(require 'grep-edit)

;; (defadvice grep-edit-change-file (around inhibit-read-only activate)
;;   ""
;;   (let ((inhibit-read-only t))
;;     ad-do-it))
;; ;; (progn (ad-disable-advice 'grep-edit-change-file 'around 'inhibit-read-only) (ad-update 'grep-edit-change-file)) 

;; (defun my-grep-edit-setup ()
;;   (define-key grep-mode-map '[up] nil)
;;   (define-key grep-mode-map "\C-c\C-c" 'grep-edit-finish-edit)
;;   (message (substitute-command-keys "\\[grep-edit-finish-edit] to apply changes."))
;;   (set (make-local-variable 'inhibit-read-only) t)
;;   )
;; (add-hook 'grep-setup-hook 'my-grep-edit-setup t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; yasnipett
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'yasnippet)
(yas/initialize)
(yas/load-directory "~/dotfiles/snippets")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; プログラミング関係
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(global-set-key "\C-x\C-t" 'ff-find-other-file)
;; ff-find-other-file 用 : 対応ヘッダーファイル、定義ファイルの拡張子の定義
(setq ff-other-file-alist
     '(("\\.mm?$" (".h"))
       ("\\.cc$"  (".hh" ".h"))
       ("\\.hh$"  (".cc" ".C"))

       ("\\.c$"   (".h"))
       ("\\.h$"   (".c" ".cc" ".C" ".CC" ".cxx" ".cpp" ".m" ".mm"))

       ("\\.C$"   (".H"  ".hh" ".h"))
       ("\\.H$"   (".C"  ".CC"))

       ("\\.CC$"  (".HH" ".H"  ".hh" ".h"))
       ("\\.HH$"  (".CC"))

       ("\\.cxx$" (".hh" ".h"))
       ("\\.cpp$" (".hpp" ".hh" ".h"))

       ("\\.hpp$" (".cpp" ".c"))))


;; cc-mode 共通フック
(add-hook 'c-mode-common-hook
	  '(lambda () 
;;	     (c-set-style "stroustrup")
	     (c-set-style "k&r")
	     (setq c-basic-offset 4)         ; 基本オフセット : インデント ４ @@@
	     (setq tab-width 4)              ; タブ長の設定
             (setq indent-tabs-mode nil)     ; インデントは空白文字で行う（TABコードを空白に変換）
             (c-set-offset 'arglist-close 0) ; 関数の引数リストの閉じ括弧はインデントしない
            ;; 対応する括弧の挿入
            (make-variable-buffer-local 'skeleton-pair)
            (make-variable-buffer-local 'skeleton-pair-on-word)
            (setq skeleton-pair-on-word t)
            (setq skeleton-pair t)
            (make-variable-buffer-local 'skeleton-pair-alist)
            (local-set-key (kbd "(") 'skeleton-pair-insert-maybe)
            (local-set-key (kbd "[") 'skeleton-pair-insert-maybe)
            (local-set-key (kbd "{") 'skeleton-pair-insert-maybe)
            (local-set-key (kbd "`") 'skeleton-pair-insert-maybe)
            (local-set-key (kbd "\"") 'skeleton-pair-insert-maybe)
  ))

;; C++ モード
(add-hook 'c++-mode-hook
          '(lambda()
             (c-set-offset 'inline-open 0)   ; Class内のinline methodの閉じ中括弧はインデントしない
             (c-set-offset 'innamespace 0)   ; namespace {}の中はインデントしない
             (define-key c-mode-base-map (kbd "C-c o") 'ff-find-other-file) ;; c-c o で対応するヘッダーファイル、定義ファイルを開く
             ))

;; Objective-C モード
(add-hook 'objc-mode-hook
         (lambda ()
           (define-key c-mode-base-map (kbd "C-c o") 'ff-find-other-file)
         ))

;; Java モード
(add-hook 'java-mode-hook
          '(lambda()
             (c-set-offset 'inline-open 0)   ; Class内のinline methodの閉じ中括弧はインデントしない
             ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  モード設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; メイクファイル
(setq auto-mode-alist
      (cons '("\.mak$" . makefile-mode) auto-mode-alist))

;; Rakefile
(setq auto-mode-alist
      (cons '("\.rake$" . ruby-mode) auto-mode-alist))
(setq auto-mode-alist
      (cons '("\Rakefile$" . ruby-mode) auto-mode-alist))

;; JAVA
(setq auto-mode-alist
      (cons '("\.jpp$" . java-mode) auto-mode-alist))

(setq auto-mode-alist
      (cons '("\.pch$" . c++-mode) auto-mode-alist))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; objc-mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; モード設定 : ファイルオープン時にファイル内をチェックして
(add-to-list 'magic-mode-alist '("\\(.\\|\n\\)*\n@implementation" . objc-mode))
(add-to-list 'magic-mode-alist '("\\(.\\|\n\\)*\n@interface" . objc-mode))
(add-to-list 'magic-mode-alist '("\\(.\\|\n\\)*\n@protocol" . objc-mode))

;; Objective-C++ : 専用モードがないので、とりあえず
(setq auto-mode-alist
      (cons '("\.mm$" . objc-mode) auto-mode-alist))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; csharp-mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(autoload 'csharp-mode "csharp-mode" "Major mode for editing C# code." t)
(setq auto-mode-alist
      (cons '("\\.cs$" . csharp-mode) auto-mode-alist))                                          

;; Patterns for finding Microsoft C# compiler error messages:                                    
(require 'compile)
(push '("^\\(.*\\)(\\([0-9]+\\),\\([0-9]+\\)): error" 1 2 3 2) compilation-error-regexp-alist)
(push '("^\\(.*\\)(\\([0-9]+\\),\\([0-9]+\\)): warning" 1 2 3 1) compilation-error-regexp-alist)

;; Patterns for defining blocks to hide/show:                                                    
(push '(csharp-mode
        "\\(^\\s *#\\s *region\\b\\)\\|{"
        "\\(^\\s *#\\s *endregion\\b\\)\\|}"
        "/[*/]"
        nil
        hs-c-like-adjust-block-beginning)
      hs-special-modes-alist)

(add-hook 'csharp-mode-hook
          '(lambda()
             (c-set-style "stroustrup")
	     (setq c-basic-offset 4)         ; 基本オフセット : インデント ４
             (setq tab-width 4)              ; タブ長の設定
             (setq indent-tabs-mode nil)     ; インデントは空白文字で行う（TABコードを空白に変換）
             (c-set-offset 'arglist-close 0) ; 関数の引数リストの閉じ括弧はインデントしない
             (c-set-offset 'inline-open 0)   ; Class内のinline methodの閉じ中括弧はインデントしない
             (setq c-auto-newline nil)
             (setq abbrev-mode nil)
             ;;(c-set-offset 'innamespace 0)   ; namespace {}の中はインデントしない
             ;;(c-set-offset 'arglist-close 0) ; 関数の引数リストの閉じ括弧はインデントしない
             )) 

;; C# ソースコードで自動有効
;;(setq auto-mode-alist
;;      (cons '("\.cs$" . auto-complete-mode) auto-mode-alist))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  ruby-mode : ruby 言語ソースコード用
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(autoload 'ruby-mode "ruby-mode"
  "Mode for editing ruby source files")
(setq auto-mode-alist
      (append '(("\\.rb$" . ruby-mode)) auto-mode-alist))
(setq interpreter-mode-alist (append '(("ruby" . ruby-mode))
                                     interpreter-mode-alist))
(autoload 'run-ruby "inf-ruby"
  "Run an inferior Ruby process")
(autoload 'inf-ruby-keys "inf-ruby"
  "set local key defs for inf-ruby in ruby-mode")
(add-hook 'ruby-mode-hook
          '(lambda ()
             (inf-ruby-keys)
             (setq tab-width 4)              ; タブ長の設定
          ))
(setq ruby-indent-level 4)

(autoload 'rubydb "rubydb3x"
  "run rubydb on program file in buffer *gud-file*.
the directory containing file becomes the initial working directory
and source-file directory for your debugger." t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  actionscript-mode : ActionScript 言語ソースコードモード
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(autoload 'actionscript-mode "actionscript-mode" "ActionScript" t)
(setq auto-mode-alist
      (append '(("\\.as$" . actionscript-mode)) auto-mode-alist))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PSVN
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq process-coding-system-alist '(("svn" . shift-jis-dos)))
(setq default-file-name-coding-system 'shift-jis-dos)
(setq svn-status-svn-file-coding-system 'shift-jis-dos)
;(setq svn-status-svn-process-coding-system 'utf-8)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; GNU global(gtags)の設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(when (locate-library "gtags") (require 'gtags))
(global-set-key "\M-t" 'gtags-find-tag)     ;関数の定義元へ
(global-set-key "\M-r" 'gtags-find-rtag)    ;関数の参照先へ
(global-set-key "\M-s" 'gtags-find-symbol)  ;変数の定義元/参照先へ
(global-set-key "\M-p" 'gtags-find-pattern)
;;(global-set-key "\M-f" 'gtags-find-file)    ;ファイルにジャンプ
(global-set-key "\C-t" 'gtags-pop-stack)   ;前のバッファに戻る
(add-hook 'c-mode-common-hook
          '(lambda ()
             (gtags-mode 1)
             (gtags-make-complete-list)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  auto-insert-mode 用設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'autoinsert)
;; テンプレート格納用ディレクトリ
(setq auto-insert-directory "~/dotfiles/insert/")
;; ファイル拡張子とテンプレートの対応
(setq auto-insert-alist
      (append '(
;;               ("\\.cpp$" . ["template.cpp" my-template])
               ("\\.hpp$" . ["template.hpp" my-template])
              ) auto-insert-alist))
(add-hook 'find-file-hooks 'auto-insert)

;; 雛形ファイルに記述するマクロの定義 
(require 'cl)
(defvar template-replacements-alists
  '(("%file%" . (lambda () (file-name-nondirectory (buffer-file-name))))
    ("%file-without-ext%" . (lambda () 
          (setq file-without-ext (file-name-sans-extension
                                   (file-name-nondirectory (buffer-file-name))))))
    ("%namespace%" .
         (lambda () (setq namespace (read-from-minibuffer "namespace: "))))
    ("%include%" .
         (lambda () 
           (cond ((string= namespace "") (concat "\"" file-without-ext ".h\""))
                 (t (concat "<" (replace-regexp-in-string "::" "/" namespace) "/"
                            file-without-ext ".h>")))))
    ("%include-guard%" . 
         (lambda ()
           (format "_%s_H_"
                   (upcase (concat 
                             (replace-regexp-in-string "::" "_" namespace)
                             (unless (string= namespace "") "_")
                             file-without-ext)))))
    ("%name%" . user-full-name)
    ("%mail%" . (lambda () (identity user-mail-address)))
    ("%cyear%" . (lambda () (substring (current-time-string) -4)))
;;    ("%bdesc%" . (lambda () (read-from-minibuffer "Brief description: ")))
    ("%namespace-open%" .
       (lambda ()
         (cond ((string= namespace "") "")
               (t (progn 
                   (setq namespace-list (split-string namespace "::"))
                   (setq namespace-text "")
                   (while namespace-list
                     (setq namespace-text (concat namespace-text "namespace "
                                                 (car namespace-list) " {\n"))
                     (setq namespace-list (cdr namespace-list))
                   )
                   (eval namespace-text))))))
    ("%namespace-close%" .
       (lambda ()
         (cond ((string= namespace "") "")
               (t (progn
                   (setq namespace-list (reverse (split-string namespace "::")))
                   (setq namespace-text "")
                   (while namespace-list
                      (setq namespace-text (concat namespace-text "} // " (car namespace-list) "\n"))
                      (setq namespace-list (cdr namespace-list))
                   )
                   (eval namespace-text))))))
))

;; 雛形ファイル中のマクロを展開する定義 
(defun my-template ()
  (time-stamp)
  (mapc #'(lambda(c)
            (progn
              (goto-char (point-min))
              (replace-string (car c) (funcall (cdr c)) nil)))
        template-replacements-alists)
  (goto-char (point-max))
  (message "done."))

(add-hook 'find-file-not-found-hooks 'auto-insert)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  以下はマクロ
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; ファイルヘッダを挿入する
(defun my-insert-fileheader-javadoc ()
  (interactive)
  (insert
   "/**\n"
   " * @file    "(file-name-nondirectory (buffer-file-name))"\n"
   " * @brief\n"
   " * \n"
   " * @author  $Author$\n"
   " * @date    $Date$\n"
   " * @version $Rev$\n"
   " */"
   ))
;;; C-x c f で ファイルヘッダ挿入
(define-key ctl-x-map "cf" 'my-insert-fileheader-javadoc)

;;; 関数ヘッダを挿入する
(defun my-insert-functionheader-javadoc ()
  (interactive)
  (insert
;; sehm 仕様
;;    "/**\n"
;;    " * @param\n"
;;    " * @return\n"
;;    " */"
;; kasshi 仕様
   "/**\n"
   " * @brief\n"
   " */"
   ))
;;; C-x c m で 関数ヘッダ挿入
(define-key ctl-x-map "cm" 'my-insert-functionheader-javadoc)


;;; ヘッダの多重インクルードを防止するアレを挿入する
;;; #define～#endifの間に空行を3行いれてその中間にカーソルを移動さす
(defun my-insert-headerregister-javadoc ()
  (interactive)
  (let ((def (concat "_" (my-replace-match (upcase (file-name-nondirectory (buffer-file-name))) "." "_") "_")))
    (insert
     "\n"
     "#ifndef " def "\n"
     "#define " def "\n"
     "\n"
     "\n"
     "\n"
     "#endif//" def "\n"
     )
    (previous-line 3)
    ))
;;; C-x c h で 多重インクルード防止挿入
(define-key ctl-x-map "ch" 'my-insert-headerregister-javadoc)


;;; temp-bufferを利用し任意の文字列の単純な置換を行う
(defun my-replace-match (string tokun replace)
  (interactive)
  (with-temp-buffer
    (insert string)
    (goto-char (point-min))
    (while (search-forward tokun nil t)
      (replace-match replace))
    (buffer-string)
    )
  )

;;;
;;; end of file
;;;
(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(recentf-max-saved-items 300))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )

(put 'erase-buffer 'disabled nil)
