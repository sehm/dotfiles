
#ifndef %include-guard%
#define %include-guard%

//namespace %namespace% {

/** クラス : %file-without-ext%
 */
class %file-without-ext%
{
public:
    /// コンストラクタ
    %file-without-ext%(){}

    /// デストラクタ
    ~%file-without-ext%(){}


    // TODO :

private:
    // 禁止
    %file-without-ext%(const %file-without-ext%&);
    %file-without-ext%& operator=(const %file-without-ext%&);
};

//}//namespace %namespace%

#endif
